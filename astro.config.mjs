import { defineConfig } from 'astro/config';
import starlight from '@astrojs/starlight';

// https://astro.build/config
export default defineConfig({
  site: 'https://docs.techbranch.site',
  outDir: 'public',
  publicDir: 'static',
  devToolbar: { enabled: false },
  integrations: [
    starlight({
      title: 'Home Lab Docs',
      social: {
        gitlab: 'https://gitlab.com/home-server-public/documentation',
      },
      sidebar: [
        {
          label: 'Guides',
          items: [
            // Each item here is one entry in the navigation menu.
            {
              label: 'Requirements',
              link: '/guides/prerequisites/requirements',
            },
            {
              label: 'Infrastructure Setup',
              items: [
                {
                  label: 'Install Microk8s',
                  link: '/guides/prerequisites/pages/install-microk8s',
                },
                {
                  label: 'Configure Local Image Registry',
                  link: '/guides/prerequisites/pages/configure-local-image-registry',
                },
                {
                  label: 'Install Terraform',
                  link: '/guides/prerequisites/pages/install-terraform',
                },
              ],
            },
          ],
        },
        {
          label: 'Projects & Source Code',
          items: [
            {
              label: 'Available Projects',
              link: '/guides/source/download',
            },
          ],
        },
        {
          label: 'Troubleshoot',
          items: [
            {
              label: 'Troubleshoot Common Issues',
              link: '/guides/troubleshooting',
            },
            {
              label: 'Guides',
              items: [
                {
                  label: 'VirtualBox',
                  link: '/guides/troubleshooting/virtualbox',
                },
                {
                  label: 'Gitlab',
                  link: '/guides/troubleshooting/gitlab',
                },
              ],
            },
          ],
        },
        {
          label: 'Reference',
          autogenerate: { directory: 'reference' },
        },
      ],
    }),
  ],
});
