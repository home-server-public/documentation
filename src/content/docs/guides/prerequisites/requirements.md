---
title: System Requirements
description: A guide for setting up your infrastructure.
---

In this section you will see a list of requisites to be installed on your system before you can continue

## System Requirements

- Ubuntu Server
- Snap
- Microk8s latest
- Terraform 1.5+

[//]: # (- Landscape &#40;optional&#41;)

For this guide we will assume that Ubuntu Server is already setup
and running on your machine.