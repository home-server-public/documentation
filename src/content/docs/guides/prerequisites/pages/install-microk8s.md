---
title: Preparing Microk8s
description: A guide for setting up Microk8s.
---

## About

This guide will walk you through setting up your infrastructure, whether it is baremetal or a virtual machine.

## Getting Started

We will start by setting up and preparing Microk8s on your system

### Installing Microk8s

Microk8s can be installed using snap, it usually comes preinstalled on Ubuntu Server.
If you don't have it installed please refer to [this guide](https://snapcraft.io/docs/installing-snap-on-ubuntu)

To install Microk8s, from your terminal run:

```bash
sudo snap install microk8s --classic --channel=1.28
```

Join the group of microk8s to be able to run microk8s commands

```bash
sudo usermod -a -G microk8s $USER && sudo chown -f -R $USER ~/.kube
```

MicroK8s uses a namespaced kubectl command to prevent conflicts with any existing installs of kubectl. If you don’t have an existing install, it is easier to add an alias (append to ~/.bash_aliases) like this:

```bash
alias kubectl='microk8s kubectl'
```

### Enabling Addons

There are few addons that are required before we can start provisioning our services, the list of addons is:

- cert-manager
- community
  - nfs
- dns
- ha-cluster
- helm3
- ingress
- metrics-server

To enable addons run the following:

```bash
microk8s.enable cert-manager community dns ha-cluster helm3 ingress metrics-server
```

#### Enabling NFS on all Nodes

Before we can enable the NFS addon we need to make sure NFS is supported on all nodes. To do so install `nfs-common` package

```bash
sudo apt install -y nfs-common
```

After the command completes, enable nfs addon by running the following

```bash
microk8s.enable nfs
```

That's it for Microk8s. You are now ready to continue installing other requirements!
