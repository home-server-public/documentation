---
title: Preparing Terraform
description: A guide for setting up Terraform.
---

## About

This guide will walk you through setting up Terraform for provisioning your services

## Getting Started

We will start by setting up Terraform on your system

### Installing Terraform

Ensure that your system is up to date and you have installed the gnupg, software-properties-common, and curl packages installed.
You will use these packages to verify HashiCorp's GPG signature and install HashiCorp's Debian package repository.

From your terminal run:

```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
```

Install the HashiCorp GPG key.

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
```

Verify the key's fingerprint.

```bash
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
```

Add the official HashiCorp repository to your system.
The lsb_release -cs command finds the distribution release codename for your current system, such as buster, groovy, or sid.

```bash
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
```

Download the package information from HashiCorp.

```bash
sudo apt update
```

Install Terraform from the new repository.

```bash
sudo apt-get install terraform
```
