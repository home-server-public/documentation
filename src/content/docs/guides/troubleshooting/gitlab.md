---
title: Troubleshoot VirtualBox
description: Troubleshoot common errors
---

This section includes troubleshooting some of the common issues I faced when running Gitlab on K8S.

## What went wrong

Running Gitlab on k8s cluster with storage class `nfs`

PostgreSQL & Redis internal instances failed to connect to socket (acquire socket lock)

Web server failed to also acquire socket lock.

## How to solve the issue

1. Make sure `no_root_squash` applied to NFS folder
2. Run the pod in `privileged = true` security context
3. PostgreSQL & Redis moved to run in their own external instances
   1. Configured gitlab.rb to point to the new instances using their k8s dns names (`gitlab-db.NAMESPACE.svc.cluster.local`)
   2. Apply the same for Redis
4. Webserver:
   1. Because the file was created by a user called `git`
   2. `web_server['username']` must be changed to `git` instead of `gitlab-www`
   3. Run `gitlab-ctl reconfigure` from the pod for changes to take affect
