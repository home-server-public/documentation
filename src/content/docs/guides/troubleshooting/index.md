---
title: Troubleshoot
description: Troubleshoot common errors
---

This section includes troubleshooting some of the common issues I faced when setting up the server and running the projects.
