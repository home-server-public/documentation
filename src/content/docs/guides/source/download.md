---
title: Downloading Source Code
description: A guide for setting up your infrastructure.
---

In this section you will see all the projects that I am running and their source code

![gethomepage.dev Dashboard](./../../../../assets/Server.png)

## Projects

List of projects grouped by purpose. Source code is available [here](https://gitlab.com/home-server-public)
Some projects support K8S via helm charts. However; I prefer to set things up manually and I chose to use docker images
and configure the rest of requirements. All official links in this guide are for docker.

### SysAdmin

These two are optional, NAS is my network storage server where all the K8S cluster data is stored and pods mount
persistent volumes there.
Patch Server is a simple Ubuntu Server running Landscape to handle patching my K8S Cluster nodes and controlerplane OS.

- NAS
- Patch Server

### DevOps

- Web Terminal (sshwifty) -- [Official Link](https://github.com/nirui/sshwifty)
- Database Management (CloudBeaver) -- [Official Link](https://dbeaver.com/docs/cloudbeaver/Run-Docker-Container/)
- Dev Environment (Coder Platform) -- [Official Link](https://coder.com/docs/v2/latest/install/docker)
- Code Editor (Coder Server) -- [Official Link](https://github.com/coder/code-server)

### Security

- PassBolt -- [Official Link](https://www.passbolt.com/ce/docker)

### Personal

- Finance (FireFly III) -- [Official Link](https://github.com/firefly-iii/docker)
- File Share (Pydio Cells) -- [Official Link](https://pydio.com/en/docs/cells/v2/docker)
- Inventory (Baserow) -- [Official Link](https://baserow.io/docs/installation/install-with-docker)
- Time Tracker (Kimai) -- [Official Link](https://www.kimai.org/documentation/docker.html)
