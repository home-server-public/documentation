---
title: Microk8s
description: A reference page for useful links.
---

Guides available in this documentation are from official Microk8s site

## Official Documentation

- [Installing Microk8s](https://microk8s.io/docs/getting-started)
- [Enabling Addons](https://microk8s.io/docs/addons)
- [Enabling NFS](https://microk8s.io/docs/addon-nfs)
- [Allow Insecure Image Registry](https://microk8s.io/docs/registry-private)
